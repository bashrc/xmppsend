APP=xmppsend
PREFIX?=/usr/local

all:
	gcc -std=c99 -pedantic -O3 -o xmppsend main.c -lstrophe
debug:
	gcc -std=c99 -pedantic -O3 -g -o xmppsend main.c -lstrophe
clean:
	rm ${APP}
install:
	mkdir -p ${DESTDIR}${PREFIX}/bin
	install -m 755 ${APP} ${DESTDIR}${PREFIX}/bin
	mkdir -m 755 -p ${DESTDIR}${PREFIX}/share/man/man1
	install -m 644 man/${APP}.1.gz ${DESTDIR}${PREFIX}/share/man/man1
uninstall:
	rm ${PREFIX}/bin/${APP}
	rm -f ${PREFIX}/share/man/man1/${APP}.1.gz
