/*
  xmppsend

  Copyright (C) 2018 Bob Mottram <bob@freedombone.net>

  Based upon the bot.c example from libstrophe, written by
  Matthew Wild <mwild1@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <strophe.h>

char *uuid=NULL;
char *tojid = NULL;
char *msg = NULL;
int presence_obtained=0;

int version_handler(xmpp_conn_t * const conn, xmpp_stanza_t * const stanza, void * const userdata)
{
    xmpp_stanza_t *reply, *query, *name, *version, *text;
    const char *ns;
    xmpp_ctx_t *ctx = (xmpp_ctx_t*)userdata;

    printf("Received version request from %s\n", xmpp_stanza_get_from(stanza));

    reply = xmpp_stanza_reply(stanza);
    xmpp_stanza_set_type(reply, "result");

    query = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(query, "query");
    ns = xmpp_stanza_get_ns(xmpp_stanza_get_children(stanza));
    if (ns) {
        xmpp_stanza_set_ns(query, ns);
    }

    name = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(name, "name");
    xmpp_stanza_add_child(query, name);
    xmpp_stanza_release(name);

    text = xmpp_stanza_new(ctx);
    xmpp_stanza_set_text(text, "xmppsend");
    xmpp_stanza_add_child(name, text);
    xmpp_stanza_release(text);

    version = xmpp_stanza_new(ctx);
    xmpp_stanza_set_name(version, "version");
    xmpp_stanza_add_child(query, version);
    xmpp_stanza_release(version);

    text = xmpp_stanza_new(ctx);
    xmpp_stanza_set_text(text, "1.0");
    xmpp_stanza_add_child(version, text);
    xmpp_stanza_release(text);

    xmpp_stanza_add_child(reply, query);
    xmpp_stanza_release(query);

    xmpp_send(conn, reply);
    xmpp_stanza_release(reply);
    return 1;
}

int roster_reply(xmpp_conn_t * const conn,
                 xmpp_stanza_t * const stanza,
                 void * const userdata)
{
    xmpp_stanza_t * message, * pres;
    xmpp_ctx_t *ctx = (xmpp_ctx_t *)userdata;
    char *uuid = xmpp_uuid_gen(ctx);

    message = xmpp_message_new(ctx, "headline", tojid, uuid);
    if (message) {
        xmpp_message_set_body(message, msg);
        xmpp_send(conn, message);
        xmpp_stanza_release(message);
    }

    sleep(1);

    /* Send final presence as recommended by
       https://xmpp.org/rfcs/rfc3921.html
       Section 5.1.5: Unavailable Presence */
    pres = xmpp_presence_new(ctx);
    xmpp_stanza_set_attribute(pres, "type", "unavailable");
    xmpp_send(conn, pres);
    xmpp_stanza_release(pres);

    return 0;
}

int presence_handler(xmpp_conn_t * const conn, xmpp_stanza_t * const stanza, void * const userdata)
{
    xmpp_ctx_t *ctx = (xmpp_ctx_t *)userdata;
    xmpp_stanza_t *iq, *query;
    char *uuid = xmpp_uuid_gen(ctx);

    if (presence_obtained == 0) {
        iq = xmpp_iq_new(ctx, "get", uuid);
        query = xmpp_stanza_new(ctx);
        xmpp_stanza_set_name(query, "query");
        xmpp_stanza_set_ns(query, XMPP_NS_ROSTER);
        xmpp_stanza_add_child(iq, query);
        xmpp_stanza_release(query);

        xmpp_id_handler_add(conn, roster_reply, uuid, ctx);

        xmpp_send(conn, iq);
        xmpp_stanza_release(iq);
        presence_obtained = 1;
    }
    else {
        xmpp_disconnect(conn);
    }

    return 1;
}

/* define a handler for connection events */
void conn_handler(xmpp_conn_t * const conn, const xmpp_conn_event_t status,
                  const int error, xmpp_stream_error_t * const stream_error,
                  void * const userdata)
{
    xmpp_ctx_t *ctx = (xmpp_ctx_t *)userdata;

    if (status == XMPP_CONN_CONNECT) {
        xmpp_stanza_t* pres;
        xmpp_handler_add(conn, version_handler, "jabber:iq:version", "iq", NULL, ctx);
        xmpp_handler_add(conn, presence_handler, NULL, "presence", NULL, ctx);

        /* Send initial <presence/> so that we appear online to contacts */
        pres = xmpp_presence_new(ctx);
        xmpp_send(conn, pres);
        xmpp_stanza_release(pres);
    }
    else {
        xmpp_stop(ctx);
    }
}

int main(int argc, char **argv)
{
    xmpp_ctx_t *ctx;
    xmpp_conn_t *conn;
    char *jid, *pass, *debug;
    xmpp_log_t *log;

    if (argc < 5) {
        fprintf(stderr, "Usage: xmppsend <jid> <pass> <tojid> <msg> <debug (optional)>\n\n");
        return 1;
    }

    jid = argv[1];
    pass = argv[2];
    tojid = argv[3];
    msg = argv[4];
    debug = argv[5];

    xmpp_initialize();
    if (debug) {
        log = xmpp_get_default_logger(XMPP_LEVEL_DEBUG);
        ctx = xmpp_ctx_new(NULL, log);
    }
    else {
        ctx = xmpp_ctx_new(NULL, NULL);
    }
    conn = xmpp_conn_new(ctx);
    xmpp_conn_set_jid(conn, jid);
    xmpp_conn_set_pass(conn, pass);
    xmpp_connect_client(conn, NULL, 0, conn_handler, ctx);
    xmpp_run(ctx);
    xmpp_conn_release(conn);
    xmpp_ctx_free(ctx);
    xmpp_shutdown();

    return 0;
}
