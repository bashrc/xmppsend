# xmppsend

This is a commandline utility used to send a single XMPP message. Typically it can be used for notifications.

## Installation

On a Debian based system:

``` bash
sudo apt-get install build-essential libstrophe-dev
```

Or on Arch/Parabola:

``` bash
sudo pacman -S libstrophe
```

Then build:

``` bash
make
sudo make install
```

## Usage

``` bash
xmppsend <Your JID> <Your password> <Recipient JID> <Message> <debug (optional)>
```
